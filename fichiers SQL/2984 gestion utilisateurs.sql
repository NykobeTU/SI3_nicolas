GRANT select ON articles TO public

GRANT insert, update ON fournisseur TO admin1

GRANT option, delete, update ON fournisseur, ligcde to admin2

REVOKE update ON fournisseur FROM admin1

CREATE ROLE  administrateur ;
GRANT insert, update, delete ON commandes, fournisseurs TO administrateur ;
CREATE ROLE admin1, admin2 ;
GRANT administrateur TO admin1, admin2