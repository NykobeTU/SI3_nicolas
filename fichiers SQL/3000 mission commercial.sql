2)
CREATE DATABASE IF NOT EXISTS missioncommercial;
USE missioncommercial;
CREATE TABLE clients (
ncli int primary key,
nomcli varchar(30),
prenomcli varchar(30),
adresse varchar(75),
codepostal int,
ville varchar(20),
ntel int
);
CREATE TABLE articles(
refarticle varchar(5) primary key,
nomarticle varchar(20),
prixunit double
);
CREATE TABLE commande(
ncommande int primary key,
ncli int,
datecom date,
constraint Gon foreign key(ncli)references clients(ncli)
);
CREATE TABLE lgc(
ncommande int, 
refarticle varchar(5),
quant int,
CONSTRAINT Hisoka PRIMARY KEY (ncommande, refarticle)
);
